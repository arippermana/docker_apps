package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripServiceFee;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripServiceFee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripServiceFeeRepository extends JpaRepository<TripServiceFee, Long> {

}
