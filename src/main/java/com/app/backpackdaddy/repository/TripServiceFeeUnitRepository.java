package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripServiceFeeUnit;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripServiceFeeUnit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripServiceFeeUnitRepository extends JpaRepository<TripServiceFeeUnit, Long> {

}
