package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripBaggage;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripBaggage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripBaggageRepository extends JpaRepository<TripBaggage, Long> {

}
