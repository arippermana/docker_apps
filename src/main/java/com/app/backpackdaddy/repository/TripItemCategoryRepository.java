package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripItemCategory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripItemCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripItemCategoryRepository extends JpaRepository<TripItemCategory, Long> {

}
