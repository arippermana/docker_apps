package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripPaymentMethod;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripPaymentMethod entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripPaymentMethodRepository extends JpaRepository<TripPaymentMethod, Long> {

}
