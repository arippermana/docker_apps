package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.UserBank;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the UserBank entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserBankRepository extends JpaRepository<UserBank, Long> {

    @Query("select user_bank from UserBank user_bank where user_bank.user.login = ?#{principal.username}")
    List<UserBank> findByUserIsCurrentUser();

}
