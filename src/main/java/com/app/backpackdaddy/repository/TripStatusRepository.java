package com.app.backpackdaddy.repository;

import com.app.backpackdaddy.domain.TripStatus;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TripStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TripStatusRepository extends JpaRepository<TripStatus, Long> {

}
