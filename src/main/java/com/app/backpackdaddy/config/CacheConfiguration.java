package com.app.backpackdaddy.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache("users", jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.SocialUserConnection.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Country.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Country.class.getName() + ".regions", jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Currency.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Gender.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Language.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Occupation.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Region.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Region.class.getName() + ".cities", jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.City.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Category.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripBaggage.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripPaymentMethod.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripServiceFee.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripServiceFeeUnit.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripStatus.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.TripItemCategory.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.Trip.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.UserBank.class.getName(), jcacheConfiguration);
            cm.createCache(com.app.backpackdaddy.domain.UserProfile.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
