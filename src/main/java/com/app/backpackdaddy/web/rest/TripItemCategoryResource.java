package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripItemCategory;

import com.app.backpackdaddy.repository.TripItemCategoryRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripItemCategory.
 */
@RestController
@RequestMapping("/api")
public class TripItemCategoryResource {

    private final Logger log = LoggerFactory.getLogger(TripItemCategoryResource.class);

    private static final String ENTITY_NAME = "tripItemCategory";

    private final TripItemCategoryRepository tripItemCategoryRepository;

    public TripItemCategoryResource(TripItemCategoryRepository tripItemCategoryRepository) {
        this.tripItemCategoryRepository = tripItemCategoryRepository;
    }

    /**
     * POST  /trip-item-categories : Create a new tripItemCategory.
     *
     * @param tripItemCategory the tripItemCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripItemCategory, or with status 400 (Bad Request) if the tripItemCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-item-categories")
    @Timed
    public ResponseEntity<TripItemCategory> createTripItemCategory(@RequestBody TripItemCategory tripItemCategory) throws URISyntaxException {
        log.debug("REST request to save TripItemCategory : {}", tripItemCategory);
        if (tripItemCategory.getId() != null) {
            throw new BadRequestAlertException("A new tripItemCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripItemCategory result = tripItemCategoryRepository.save(tripItemCategory);
        return ResponseEntity.created(new URI("/api/trip-item-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-item-categories : Updates an existing tripItemCategory.
     *
     * @param tripItemCategory the tripItemCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripItemCategory,
     * or with status 400 (Bad Request) if the tripItemCategory is not valid,
     * or with status 500 (Internal Server Error) if the tripItemCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-item-categories")
    @Timed
    public ResponseEntity<TripItemCategory> updateTripItemCategory(@RequestBody TripItemCategory tripItemCategory) throws URISyntaxException {
        log.debug("REST request to update TripItemCategory : {}", tripItemCategory);
        if (tripItemCategory.getId() == null) {
            return createTripItemCategory(tripItemCategory);
        }
        TripItemCategory result = tripItemCategoryRepository.save(tripItemCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripItemCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-item-categories : get all the tripItemCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripItemCategories in body
     */
    @GetMapping("/trip-item-categories")
    @Timed
    public List<TripItemCategory> getAllTripItemCategories() {
        log.debug("REST request to get all TripItemCategories");
        return tripItemCategoryRepository.findAll();
        }

    /**
     * GET  /trip-item-categories/:id : get the "id" tripItemCategory.
     *
     * @param id the id of the tripItemCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripItemCategory, or with status 404 (Not Found)
     */
    @GetMapping("/trip-item-categories/{id}")
    @Timed
    public ResponseEntity<TripItemCategory> getTripItemCategory(@PathVariable Long id) {
        log.debug("REST request to get TripItemCategory : {}", id);
        TripItemCategory tripItemCategory = tripItemCategoryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripItemCategory));
    }

    /**
     * DELETE  /trip-item-categories/:id : delete the "id" tripItemCategory.
     *
     * @param id the id of the tripItemCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-item-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripItemCategory(@PathVariable Long id) {
        log.debug("REST request to delete TripItemCategory : {}", id);
        tripItemCategoryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
