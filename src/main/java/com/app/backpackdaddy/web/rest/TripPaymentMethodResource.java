package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripPaymentMethod;

import com.app.backpackdaddy.repository.TripPaymentMethodRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripPaymentMethod.
 */
@RestController
@RequestMapping("/api")
public class TripPaymentMethodResource {

    private final Logger log = LoggerFactory.getLogger(TripPaymentMethodResource.class);

    private static final String ENTITY_NAME = "tripPaymentMethod";

    private final TripPaymentMethodRepository tripPaymentMethodRepository;

    public TripPaymentMethodResource(TripPaymentMethodRepository tripPaymentMethodRepository) {
        this.tripPaymentMethodRepository = tripPaymentMethodRepository;
    }

    /**
     * POST  /trip-payment-methods : Create a new tripPaymentMethod.
     *
     * @param tripPaymentMethod the tripPaymentMethod to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripPaymentMethod, or with status 400 (Bad Request) if the tripPaymentMethod has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-payment-methods")
    @Timed
    public ResponseEntity<TripPaymentMethod> createTripPaymentMethod(@Valid @RequestBody TripPaymentMethod tripPaymentMethod) throws URISyntaxException {
        log.debug("REST request to save TripPaymentMethod : {}", tripPaymentMethod);
        if (tripPaymentMethod.getId() != null) {
            throw new BadRequestAlertException("A new tripPaymentMethod cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripPaymentMethod result = tripPaymentMethodRepository.save(tripPaymentMethod);
        return ResponseEntity.created(new URI("/api/trip-payment-methods/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-payment-methods : Updates an existing tripPaymentMethod.
     *
     * @param tripPaymentMethod the tripPaymentMethod to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripPaymentMethod,
     * or with status 400 (Bad Request) if the tripPaymentMethod is not valid,
     * or with status 500 (Internal Server Error) if the tripPaymentMethod couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-payment-methods")
    @Timed
    public ResponseEntity<TripPaymentMethod> updateTripPaymentMethod(@Valid @RequestBody TripPaymentMethod tripPaymentMethod) throws URISyntaxException {
        log.debug("REST request to update TripPaymentMethod : {}", tripPaymentMethod);
        if (tripPaymentMethod.getId() == null) {
            return createTripPaymentMethod(tripPaymentMethod);
        }
        TripPaymentMethod result = tripPaymentMethodRepository.save(tripPaymentMethod);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripPaymentMethod.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-payment-methods : get all the tripPaymentMethods.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripPaymentMethods in body
     */
    @GetMapping("/trip-payment-methods")
    @Timed
    public List<TripPaymentMethod> getAllTripPaymentMethods() {
        log.debug("REST request to get all TripPaymentMethods");
        return tripPaymentMethodRepository.findAll();
        }

    /**
     * GET  /trip-payment-methods/:id : get the "id" tripPaymentMethod.
     *
     * @param id the id of the tripPaymentMethod to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripPaymentMethod, or with status 404 (Not Found)
     */
    @GetMapping("/trip-payment-methods/{id}")
    @Timed
    public ResponseEntity<TripPaymentMethod> getTripPaymentMethod(@PathVariable Long id) {
        log.debug("REST request to get TripPaymentMethod : {}", id);
        TripPaymentMethod tripPaymentMethod = tripPaymentMethodRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripPaymentMethod));
    }

    /**
     * DELETE  /trip-payment-methods/:id : delete the "id" tripPaymentMethod.
     *
     * @param id the id of the tripPaymentMethod to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-payment-methods/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripPaymentMethod(@PathVariable Long id) {
        log.debug("REST request to delete TripPaymentMethod : {}", id);
        tripPaymentMethodRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
