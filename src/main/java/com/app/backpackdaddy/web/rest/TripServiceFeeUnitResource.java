package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripServiceFeeUnit;

import com.app.backpackdaddy.repository.TripServiceFeeUnitRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripServiceFeeUnit.
 */
@RestController
@RequestMapping("/api")
public class TripServiceFeeUnitResource {

    private final Logger log = LoggerFactory.getLogger(TripServiceFeeUnitResource.class);

    private static final String ENTITY_NAME = "tripServiceFeeUnit";

    private final TripServiceFeeUnitRepository tripServiceFeeUnitRepository;

    public TripServiceFeeUnitResource(TripServiceFeeUnitRepository tripServiceFeeUnitRepository) {
        this.tripServiceFeeUnitRepository = tripServiceFeeUnitRepository;
    }

    /**
     * POST  /trip-service-fee-units : Create a new tripServiceFeeUnit.
     *
     * @param tripServiceFeeUnit the tripServiceFeeUnit to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripServiceFeeUnit, or with status 400 (Bad Request) if the tripServiceFeeUnit has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-service-fee-units")
    @Timed
    public ResponseEntity<TripServiceFeeUnit> createTripServiceFeeUnit(@Valid @RequestBody TripServiceFeeUnit tripServiceFeeUnit) throws URISyntaxException {
        log.debug("REST request to save TripServiceFeeUnit : {}", tripServiceFeeUnit);
        if (tripServiceFeeUnit.getId() != null) {
            throw new BadRequestAlertException("A new tripServiceFeeUnit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripServiceFeeUnit result = tripServiceFeeUnitRepository.save(tripServiceFeeUnit);
        return ResponseEntity.created(new URI("/api/trip-service-fee-units/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-service-fee-units : Updates an existing tripServiceFeeUnit.
     *
     * @param tripServiceFeeUnit the tripServiceFeeUnit to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripServiceFeeUnit,
     * or with status 400 (Bad Request) if the tripServiceFeeUnit is not valid,
     * or with status 500 (Internal Server Error) if the tripServiceFeeUnit couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-service-fee-units")
    @Timed
    public ResponseEntity<TripServiceFeeUnit> updateTripServiceFeeUnit(@Valid @RequestBody TripServiceFeeUnit tripServiceFeeUnit) throws URISyntaxException {
        log.debug("REST request to update TripServiceFeeUnit : {}", tripServiceFeeUnit);
        if (tripServiceFeeUnit.getId() == null) {
            return createTripServiceFeeUnit(tripServiceFeeUnit);
        }
        TripServiceFeeUnit result = tripServiceFeeUnitRepository.save(tripServiceFeeUnit);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripServiceFeeUnit.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-service-fee-units : get all the tripServiceFeeUnits.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripServiceFeeUnits in body
     */
    @GetMapping("/trip-service-fee-units")
    @Timed
    public List<TripServiceFeeUnit> getAllTripServiceFeeUnits() {
        log.debug("REST request to get all TripServiceFeeUnits");
        return tripServiceFeeUnitRepository.findAll();
        }

    /**
     * GET  /trip-service-fee-units/:id : get the "id" tripServiceFeeUnit.
     *
     * @param id the id of the tripServiceFeeUnit to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripServiceFeeUnit, or with status 404 (Not Found)
     */
    @GetMapping("/trip-service-fee-units/{id}")
    @Timed
    public ResponseEntity<TripServiceFeeUnit> getTripServiceFeeUnit(@PathVariable Long id) {
        log.debug("REST request to get TripServiceFeeUnit : {}", id);
        TripServiceFeeUnit tripServiceFeeUnit = tripServiceFeeUnitRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripServiceFeeUnit));
    }

    /**
     * DELETE  /trip-service-fee-units/:id : delete the "id" tripServiceFeeUnit.
     *
     * @param id the id of the tripServiceFeeUnit to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-service-fee-units/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripServiceFeeUnit(@PathVariable Long id) {
        log.debug("REST request to delete TripServiceFeeUnit : {}", id);
        tripServiceFeeUnitRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
