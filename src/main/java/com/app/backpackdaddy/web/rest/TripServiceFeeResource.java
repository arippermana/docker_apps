package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripServiceFee;

import com.app.backpackdaddy.repository.TripServiceFeeRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripServiceFee.
 */
@RestController
@RequestMapping("/api")
public class TripServiceFeeResource {

    private final Logger log = LoggerFactory.getLogger(TripServiceFeeResource.class);

    private static final String ENTITY_NAME = "tripServiceFee";

    private final TripServiceFeeRepository tripServiceFeeRepository;

    public TripServiceFeeResource(TripServiceFeeRepository tripServiceFeeRepository) {
        this.tripServiceFeeRepository = tripServiceFeeRepository;
    }

    /**
     * POST  /trip-service-fees : Create a new tripServiceFee.
     *
     * @param tripServiceFee the tripServiceFee to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripServiceFee, or with status 400 (Bad Request) if the tripServiceFee has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-service-fees")
    @Timed
    public ResponseEntity<TripServiceFee> createTripServiceFee(@Valid @RequestBody TripServiceFee tripServiceFee) throws URISyntaxException {
        log.debug("REST request to save TripServiceFee : {}", tripServiceFee);
        if (tripServiceFee.getId() != null) {
            throw new BadRequestAlertException("A new tripServiceFee cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripServiceFee result = tripServiceFeeRepository.save(tripServiceFee);
        return ResponseEntity.created(new URI("/api/trip-service-fees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-service-fees : Updates an existing tripServiceFee.
     *
     * @param tripServiceFee the tripServiceFee to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripServiceFee,
     * or with status 400 (Bad Request) if the tripServiceFee is not valid,
     * or with status 500 (Internal Server Error) if the tripServiceFee couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-service-fees")
    @Timed
    public ResponseEntity<TripServiceFee> updateTripServiceFee(@Valid @RequestBody TripServiceFee tripServiceFee) throws URISyntaxException {
        log.debug("REST request to update TripServiceFee : {}", tripServiceFee);
        if (tripServiceFee.getId() == null) {
            return createTripServiceFee(tripServiceFee);
        }
        TripServiceFee result = tripServiceFeeRepository.save(tripServiceFee);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripServiceFee.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-service-fees : get all the tripServiceFees.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripServiceFees in body
     */
    @GetMapping("/trip-service-fees")
    @Timed
    public List<TripServiceFee> getAllTripServiceFees() {
        log.debug("REST request to get all TripServiceFees");
        return tripServiceFeeRepository.findAll();
        }

    /**
     * GET  /trip-service-fees/:id : get the "id" tripServiceFee.
     *
     * @param id the id of the tripServiceFee to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripServiceFee, or with status 404 (Not Found)
     */
    @GetMapping("/trip-service-fees/{id}")
    @Timed
    public ResponseEntity<TripServiceFee> getTripServiceFee(@PathVariable Long id) {
        log.debug("REST request to get TripServiceFee : {}", id);
        TripServiceFee tripServiceFee = tripServiceFeeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripServiceFee));
    }

    /**
     * DELETE  /trip-service-fees/:id : delete the "id" tripServiceFee.
     *
     * @param id the id of the tripServiceFee to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-service-fees/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripServiceFee(@PathVariable Long id) {
        log.debug("REST request to delete TripServiceFee : {}", id);
        tripServiceFeeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
