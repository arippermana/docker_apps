package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripBaggage;

import com.app.backpackdaddy.repository.TripBaggageRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripBaggage.
 */
@RestController
@RequestMapping("/api")
public class TripBaggageResource {

    private final Logger log = LoggerFactory.getLogger(TripBaggageResource.class);

    private static final String ENTITY_NAME = "tripBaggage";

    private final TripBaggageRepository tripBaggageRepository;

    public TripBaggageResource(TripBaggageRepository tripBaggageRepository) {
        this.tripBaggageRepository = tripBaggageRepository;
    }

    /**
     * POST  /trip-baggages : Create a new tripBaggage.
     *
     * @param tripBaggage the tripBaggage to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripBaggage, or with status 400 (Bad Request) if the tripBaggage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-baggages")
    @Timed
    public ResponseEntity<TripBaggage> createTripBaggage(@Valid @RequestBody TripBaggage tripBaggage) throws URISyntaxException {
        log.debug("REST request to save TripBaggage : {}", tripBaggage);
        if (tripBaggage.getId() != null) {
            throw new BadRequestAlertException("A new tripBaggage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripBaggage result = tripBaggageRepository.save(tripBaggage);
        return ResponseEntity.created(new URI("/api/trip-baggages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-baggages : Updates an existing tripBaggage.
     *
     * @param tripBaggage the tripBaggage to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripBaggage,
     * or with status 400 (Bad Request) if the tripBaggage is not valid,
     * or with status 500 (Internal Server Error) if the tripBaggage couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-baggages")
    @Timed
    public ResponseEntity<TripBaggage> updateTripBaggage(@Valid @RequestBody TripBaggage tripBaggage) throws URISyntaxException {
        log.debug("REST request to update TripBaggage : {}", tripBaggage);
        if (tripBaggage.getId() == null) {
            return createTripBaggage(tripBaggage);
        }
        TripBaggage result = tripBaggageRepository.save(tripBaggage);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripBaggage.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-baggages : get all the tripBaggages.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripBaggages in body
     */
    @GetMapping("/trip-baggages")
    @Timed
    public List<TripBaggage> getAllTripBaggages() {
        log.debug("REST request to get all TripBaggages");
        return tripBaggageRepository.findAll();
        }

    /**
     * GET  /trip-baggages/:id : get the "id" tripBaggage.
     *
     * @param id the id of the tripBaggage to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripBaggage, or with status 404 (Not Found)
     */
    @GetMapping("/trip-baggages/{id}")
    @Timed
    public ResponseEntity<TripBaggage> getTripBaggage(@PathVariable Long id) {
        log.debug("REST request to get TripBaggage : {}", id);
        TripBaggage tripBaggage = tripBaggageRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripBaggage));
    }

    /**
     * DELETE  /trip-baggages/:id : delete the "id" tripBaggage.
     *
     * @param id the id of the tripBaggage to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-baggages/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripBaggage(@PathVariable Long id) {
        log.debug("REST request to delete TripBaggage : {}", id);
        tripBaggageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
