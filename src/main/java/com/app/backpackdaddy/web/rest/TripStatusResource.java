package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.TripStatus;

import com.app.backpackdaddy.repository.TripStatusRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TripStatus.
 */
@RestController
@RequestMapping("/api")
public class TripStatusResource {

    private final Logger log = LoggerFactory.getLogger(TripStatusResource.class);

    private static final String ENTITY_NAME = "tripStatus";

    private final TripStatusRepository tripStatusRepository;

    public TripStatusResource(TripStatusRepository tripStatusRepository) {
        this.tripStatusRepository = tripStatusRepository;
    }

    /**
     * POST  /trip-statuses : Create a new tripStatus.
     *
     * @param tripStatus the tripStatus to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tripStatus, or with status 400 (Bad Request) if the tripStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trip-statuses")
    @Timed
    public ResponseEntity<TripStatus> createTripStatus(@Valid @RequestBody TripStatus tripStatus) throws URISyntaxException {
        log.debug("REST request to save TripStatus : {}", tripStatus);
        if (tripStatus.getId() != null) {
            throw new BadRequestAlertException("A new tripStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TripStatus result = tripStatusRepository.save(tripStatus);
        return ResponseEntity.created(new URI("/api/trip-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /trip-statuses : Updates an existing tripStatus.
     *
     * @param tripStatus the tripStatus to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tripStatus,
     * or with status 400 (Bad Request) if the tripStatus is not valid,
     * or with status 500 (Internal Server Error) if the tripStatus couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trip-statuses")
    @Timed
    public ResponseEntity<TripStatus> updateTripStatus(@Valid @RequestBody TripStatus tripStatus) throws URISyntaxException {
        log.debug("REST request to update TripStatus : {}", tripStatus);
        if (tripStatus.getId() == null) {
            return createTripStatus(tripStatus);
        }
        TripStatus result = tripStatusRepository.save(tripStatus);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tripStatus.getId().toString()))
            .body(result);
    }

    /**
     * GET  /trip-statuses : get all the tripStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tripStatuses in body
     */
    @GetMapping("/trip-statuses")
    @Timed
    public List<TripStatus> getAllTripStatuses() {
        log.debug("REST request to get all TripStatuses");
        return tripStatusRepository.findAll();
        }

    /**
     * GET  /trip-statuses/:id : get the "id" tripStatus.
     *
     * @param id the id of the tripStatus to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tripStatus, or with status 404 (Not Found)
     */
    @GetMapping("/trip-statuses/{id}")
    @Timed
    public ResponseEntity<TripStatus> getTripStatus(@PathVariable Long id) {
        log.debug("REST request to get TripStatus : {}", id);
        TripStatus tripStatus = tripStatusRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tripStatus));
    }

    /**
     * DELETE  /trip-statuses/:id : delete the "id" tripStatus.
     *
     * @param id the id of the tripStatus to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trip-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteTripStatus(@PathVariable Long id) {
        log.debug("REST request to delete TripStatus : {}", id);
        tripStatusRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
