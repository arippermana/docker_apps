package com.app.backpackdaddy.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.app.backpackdaddy.domain.UserBank;

import com.app.backpackdaddy.repository.UserBankRepository;
import com.app.backpackdaddy.web.rest.errors.BadRequestAlertException;
import com.app.backpackdaddy.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserBank.
 */
@RestController
@RequestMapping("/api")
public class UserBankResource {

    private final Logger log = LoggerFactory.getLogger(UserBankResource.class);

    private static final String ENTITY_NAME = "userBank";

    private final UserBankRepository userBankRepository;

    public UserBankResource(UserBankRepository userBankRepository) {
        this.userBankRepository = userBankRepository;
    }

    /**
     * POST  /user-banks : Create a new userBank.
     *
     * @param userBank the userBank to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userBank, or with status 400 (Bad Request) if the userBank has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-banks")
    @Timed
    public ResponseEntity<UserBank> createUserBank(@Valid @RequestBody UserBank userBank) throws URISyntaxException {
        log.debug("REST request to save UserBank : {}", userBank);
        if (userBank.getId() != null) {
            throw new BadRequestAlertException("A new userBank cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserBank result = userBankRepository.save(userBank);
        return ResponseEntity.created(new URI("/api/user-banks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-banks : Updates an existing userBank.
     *
     * @param userBank the userBank to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userBank,
     * or with status 400 (Bad Request) if the userBank is not valid,
     * or with status 500 (Internal Server Error) if the userBank couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-banks")
    @Timed
    public ResponseEntity<UserBank> updateUserBank(@Valid @RequestBody UserBank userBank) throws URISyntaxException {
        log.debug("REST request to update UserBank : {}", userBank);
        if (userBank.getId() == null) {
            return createUserBank(userBank);
        }
        UserBank result = userBankRepository.save(userBank);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userBank.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-banks : get all the userBanks.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userBanks in body
     */
    @GetMapping("/user-banks")
    @Timed
    public List<UserBank> getAllUserBanks() {
        log.debug("REST request to get all UserBanks");
        return userBankRepository.findAll();
        }

    /**
     * GET  /user-banks/:id : get the "id" userBank.
     *
     * @param id the id of the userBank to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userBank, or with status 404 (Not Found)
     */
    @GetMapping("/user-banks/{id}")
    @Timed
    public ResponseEntity<UserBank> getUserBank(@PathVariable Long id) {
        log.debug("REST request to get UserBank : {}", id);
        UserBank userBank = userBankRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userBank));
    }

    /**
     * DELETE  /user-banks/:id : delete the "id" userBank.
     *
     * @param id the id of the userBank to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-banks/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserBank(@PathVariable Long id) {
        log.debug("REST request to delete UserBank : {}", id);
        userBankRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
