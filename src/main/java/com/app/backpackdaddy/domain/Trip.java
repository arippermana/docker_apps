package com.app.backpackdaddy.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Trip.
 */
@Entity
@Table(name = "trip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Trip implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "trip_date", nullable = false)
    private LocalDate tripDate;

    @Column(name = "baggage_maks")
    private Integer baggageMaks;

    @NotNull
    @Column(name = "service_fee_min", nullable = false)
    private Integer serviceFeeMin;

    @Column(name = "service_fee_max")
    private Integer serviceFeeMax;

    @NotNull
    @Size(max = 255)
    @Column(name = "additional_info", length = 255, nullable = false)
    private String additionalInfo;

    @ManyToOne
    private User user;

    @ManyToOne
    private City tripFromCity;

    @ManyToOne
    private City tripToCity;

    @ManyToOne
    private TripBaggage baggage;

    @ManyToOne
    private TripPaymentMethod paymentMethod;

    @ManyToOne
    private UserBank bankAccount;

    @ManyToOne
    private TripServiceFee serviceFee;

    @ManyToOne
    private TripServiceFeeUnit serviceFeeUnit;

    @ManyToOne
    private TripStatus status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTripDate() {
        return tripDate;
    }

    public Trip tripDate(LocalDate tripDate) {
        this.tripDate = tripDate;
        return this;
    }

    public void setTripDate(LocalDate tripDate) {
        this.tripDate = tripDate;
    }

    public Integer getBaggageMaks() {
        return baggageMaks;
    }

    public Trip baggageMaks(Integer baggageMaks) {
        this.baggageMaks = baggageMaks;
        return this;
    }

    public void setBaggageMaks(Integer baggageMaks) {
        this.baggageMaks = baggageMaks;
    }

    public Integer getServiceFeeMin() {
        return serviceFeeMin;
    }

    public Trip serviceFeeMin(Integer serviceFeeMin) {
        this.serviceFeeMin = serviceFeeMin;
        return this;
    }

    public void setServiceFeeMin(Integer serviceFeeMin) {
        this.serviceFeeMin = serviceFeeMin;
    }

    public Integer getServiceFeeMax() {
        return serviceFeeMax;
    }

    public Trip serviceFeeMax(Integer serviceFeeMax) {
        this.serviceFeeMax = serviceFeeMax;
        return this;
    }

    public void setServiceFeeMax(Integer serviceFeeMax) {
        this.serviceFeeMax = serviceFeeMax;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public Trip additionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public User getUser() {
        return user;
    }

    public Trip user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public City getTripFromCity() {
        return tripFromCity;
    }

    public Trip tripFromCity(City city) {
        this.tripFromCity = city;
        return this;
    }

    public void setTripFromCity(City city) {
        this.tripFromCity = city;
    }

    public City getTripToCity() {
        return tripToCity;
    }

    public Trip tripToCity(City city) {
        this.tripToCity = city;
        return this;
    }

    public void setTripToCity(City city) {
        this.tripToCity = city;
    }

    public TripBaggage getBaggage() {
        return baggage;
    }

    public Trip baggage(TripBaggage tripBaggage) {
        this.baggage = tripBaggage;
        return this;
    }

    public void setBaggage(TripBaggage tripBaggage) {
        this.baggage = tripBaggage;
    }

    public TripPaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Trip paymentMethod(TripPaymentMethod tripPaymentMethod) {
        this.paymentMethod = tripPaymentMethod;
        return this;
    }

    public void setPaymentMethod(TripPaymentMethod tripPaymentMethod) {
        this.paymentMethod = tripPaymentMethod;
    }

    public UserBank getBankAccount() {
        return bankAccount;
    }

    public Trip bankAccount(UserBank userBank) {
        this.bankAccount = userBank;
        return this;
    }

    public void setBankAccount(UserBank userBank) {
        this.bankAccount = userBank;
    }

    public TripServiceFee getServiceFee() {
        return serviceFee;
    }

    public Trip serviceFee(TripServiceFee tripServiceFee) {
        this.serviceFee = tripServiceFee;
        return this;
    }

    public void setServiceFee(TripServiceFee tripServiceFee) {
        this.serviceFee = tripServiceFee;
    }

    public TripServiceFeeUnit getServiceFeeUnit() {
        return serviceFeeUnit;
    }

    public Trip serviceFeeUnit(TripServiceFeeUnit tripServiceFeeUnit) {
        this.serviceFeeUnit = tripServiceFeeUnit;
        return this;
    }

    public void setServiceFeeUnit(TripServiceFeeUnit tripServiceFeeUnit) {
        this.serviceFeeUnit = tripServiceFeeUnit;
    }

    public TripStatus getStatus() {
        return status;
    }

    public Trip status(TripStatus tripStatus) {
        this.status = tripStatus;
        return this;
    }

    public void setStatus(TripStatus tripStatus) {
        this.status = tripStatus;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Trip trip = (Trip) o;
        if (trip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Trip{" +
            "id=" + getId() +
            ", tripDate='" + getTripDate() + "'" +
            ", baggageMaks=" + getBaggageMaks() +
            ", serviceFeeMin=" + getServiceFeeMin() +
            ", serviceFeeMax=" + getServiceFeeMax() +
            ", additionalInfo='" + getAdditionalInfo() + "'" +
            "}";
    }
}
