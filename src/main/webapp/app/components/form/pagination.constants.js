(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
