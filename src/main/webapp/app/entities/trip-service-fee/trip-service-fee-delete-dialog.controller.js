(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeDeleteController',TripServiceFeeDeleteController);

    TripServiceFeeDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripServiceFee'];

    function TripServiceFeeDeleteController($uibModalInstance, entity, TripServiceFee) {
        var vm = this;

        vm.tripServiceFee = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripServiceFee.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
