(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeDialogController', TripServiceFeeDialogController);

    TripServiceFeeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripServiceFee'];

    function TripServiceFeeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripServiceFee) {
        var vm = this;

        vm.tripServiceFee = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripServiceFee.id !== null) {
                TripServiceFee.update(vm.tripServiceFee, onSaveSuccess, onSaveError);
            } else {
                TripServiceFee.save(vm.tripServiceFee, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripServiceFeeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
