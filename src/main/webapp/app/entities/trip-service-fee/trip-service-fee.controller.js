(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeController', TripServiceFeeController);

    TripServiceFeeController.$inject = ['TripServiceFee'];

    function TripServiceFeeController(TripServiceFee) {

        var vm = this;

        vm.tripServiceFees = [];

        loadAll();

        function loadAll() {
            TripServiceFee.query(function(result) {
                vm.tripServiceFees = result;
                vm.searchQuery = null;
            });
        }
    }
})();
