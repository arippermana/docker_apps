(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeDetailController', TripServiceFeeDetailController);

    TripServiceFeeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripServiceFee'];

    function TripServiceFeeDetailController($scope, $rootScope, $stateParams, previousState, entity, TripServiceFee) {
        var vm = this;

        vm.tripServiceFee = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripServiceFeeUpdate', function(event, result) {
            vm.tripServiceFee = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
