(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-service-fee', {
            parent: 'entity',
            url: '/trip-service-fee',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripServiceFee.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fees.html',
                    controller: 'TripServiceFeeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripServiceFee');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-service-fee-detail', {
            parent: 'trip-service-fee',
            url: '/trip-service-fee/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripServiceFee.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fee-detail.html',
                    controller: 'TripServiceFeeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripServiceFee');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripServiceFee', function($stateParams, TripServiceFee) {
                    return TripServiceFee.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-service-fee',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-service-fee-detail.edit', {
            parent: 'trip-service-fee-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fee-dialog.html',
                    controller: 'TripServiceFeeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripServiceFee', function(TripServiceFee) {
                            return TripServiceFee.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-service-fee.new', {
            parent: 'trip-service-fee',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fee-dialog.html',
                    controller: 'TripServiceFeeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee', null, { reload: 'trip-service-fee' });
                }, function() {
                    $state.go('trip-service-fee');
                });
            }]
        })
        .state('trip-service-fee.edit', {
            parent: 'trip-service-fee',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fee-dialog.html',
                    controller: 'TripServiceFeeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripServiceFee', function(TripServiceFee) {
                            return TripServiceFee.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee', null, { reload: 'trip-service-fee' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-service-fee.delete', {
            parent: 'trip-service-fee',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee/trip-service-fee-delete-dialog.html',
                    controller: 'TripServiceFeeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripServiceFee', function(TripServiceFee) {
                            return TripServiceFee.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee', null, { reload: 'trip-service-fee' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
