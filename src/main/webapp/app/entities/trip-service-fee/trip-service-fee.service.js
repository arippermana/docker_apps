(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripServiceFee', TripServiceFee);

    TripServiceFee.$inject = ['$resource'];

    function TripServiceFee ($resource) {
        var resourceUrl =  'api/trip-service-fees/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
