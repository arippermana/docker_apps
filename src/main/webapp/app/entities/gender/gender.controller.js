(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('GenderController', GenderController);

    GenderController.$inject = ['Gender'];

    function GenderController(Gender) {

        var vm = this;

        vm.genders = [];

        loadAll();

        function loadAll() {
            Gender.query(function(result) {
                vm.genders = result;
                vm.searchQuery = null;
            });
        }
    }
})();
