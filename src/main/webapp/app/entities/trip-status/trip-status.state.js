(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-status', {
            parent: 'entity',
            url: '/trip-status',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripStatus.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-status/trip-statuses.html',
                    controller: 'TripStatusController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-status-detail', {
            parent: 'trip-status',
            url: '/trip-status/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripStatus.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-status/trip-status-detail.html',
                    controller: 'TripStatusDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripStatus', function($stateParams, TripStatus) {
                    return TripStatus.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-status',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-status-detail.edit', {
            parent: 'trip-status-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-status/trip-status-dialog.html',
                    controller: 'TripStatusDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripStatus', function(TripStatus) {
                            return TripStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-status.new', {
            parent: 'trip-status',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-status/trip-status-dialog.html',
                    controller: 'TripStatusDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-status', null, { reload: 'trip-status' });
                }, function() {
                    $state.go('trip-status');
                });
            }]
        })
        .state('trip-status.edit', {
            parent: 'trip-status',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-status/trip-status-dialog.html',
                    controller: 'TripStatusDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripStatus', function(TripStatus) {
                            return TripStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-status', null, { reload: 'trip-status' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-status.delete', {
            parent: 'trip-status',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-status/trip-status-delete-dialog.html',
                    controller: 'TripStatusDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripStatus', function(TripStatus) {
                            return TripStatus.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-status', null, { reload: 'trip-status' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
