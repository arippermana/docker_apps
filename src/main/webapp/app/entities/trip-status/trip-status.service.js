(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripStatus', TripStatus);

    TripStatus.$inject = ['$resource'];

    function TripStatus ($resource) {
        var resourceUrl =  'api/trip-statuses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
