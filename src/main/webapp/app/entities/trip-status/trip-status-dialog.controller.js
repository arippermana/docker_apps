(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripStatusDialogController', TripStatusDialogController);

    TripStatusDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripStatus'];

    function TripStatusDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripStatus) {
        var vm = this;

        vm.tripStatus = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripStatus.id !== null) {
                TripStatus.update(vm.tripStatus, onSaveSuccess, onSaveError);
            } else {
                TripStatus.save(vm.tripStatus, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripStatusUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
