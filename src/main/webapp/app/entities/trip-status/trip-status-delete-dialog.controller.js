(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripStatusDeleteController',TripStatusDeleteController);

    TripStatusDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripStatus'];

    function TripStatusDeleteController($uibModalInstance, entity, TripStatus) {
        var vm = this;

        vm.tripStatus = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripStatus.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
