(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripStatusController', TripStatusController);

    TripStatusController.$inject = ['TripStatus'];

    function TripStatusController(TripStatus) {

        var vm = this;

        vm.tripStatuses = [];

        loadAll();

        function loadAll() {
            TripStatus.query(function(result) {
                vm.tripStatuses = result;
                vm.searchQuery = null;
            });
        }
    }
})();
