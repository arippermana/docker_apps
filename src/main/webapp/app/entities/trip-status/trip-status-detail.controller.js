(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripStatusDetailController', TripStatusDetailController);

    TripStatusDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripStatus'];

    function TripStatusDetailController($scope, $rootScope, $stateParams, previousState, entity, TripStatus) {
        var vm = this;

        vm.tripStatus = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripStatusUpdate', function(event, result) {
            vm.tripStatus = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
