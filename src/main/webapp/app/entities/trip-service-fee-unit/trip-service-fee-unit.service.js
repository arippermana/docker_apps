(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripServiceFeeUnit', TripServiceFeeUnit);

    TripServiceFeeUnit.$inject = ['$resource'];

    function TripServiceFeeUnit ($resource) {
        var resourceUrl =  'api/trip-service-fee-units/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
