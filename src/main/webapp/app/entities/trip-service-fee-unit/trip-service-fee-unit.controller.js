(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeUnitController', TripServiceFeeUnitController);

    TripServiceFeeUnitController.$inject = ['TripServiceFeeUnit'];

    function TripServiceFeeUnitController(TripServiceFeeUnit) {

        var vm = this;

        vm.tripServiceFeeUnits = [];

        loadAll();

        function loadAll() {
            TripServiceFeeUnit.query(function(result) {
                vm.tripServiceFeeUnits = result;
                vm.searchQuery = null;
            });
        }
    }
})();
