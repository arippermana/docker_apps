(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeUnitDetailController', TripServiceFeeUnitDetailController);

    TripServiceFeeUnitDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripServiceFeeUnit'];

    function TripServiceFeeUnitDetailController($scope, $rootScope, $stateParams, previousState, entity, TripServiceFeeUnit) {
        var vm = this;

        vm.tripServiceFeeUnit = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripServiceFeeUnitUpdate', function(event, result) {
            vm.tripServiceFeeUnit = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
