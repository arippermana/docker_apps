(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-service-fee-unit', {
            parent: 'entity',
            url: '/trip-service-fee-unit',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripServiceFeeUnit.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-units.html',
                    controller: 'TripServiceFeeUnitController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripServiceFeeUnit');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-service-fee-unit-detail', {
            parent: 'trip-service-fee-unit',
            url: '/trip-service-fee-unit/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripServiceFeeUnit.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-unit-detail.html',
                    controller: 'TripServiceFeeUnitDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripServiceFeeUnit');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripServiceFeeUnit', function($stateParams, TripServiceFeeUnit) {
                    return TripServiceFeeUnit.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-service-fee-unit',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-service-fee-unit-detail.edit', {
            parent: 'trip-service-fee-unit-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-unit-dialog.html',
                    controller: 'TripServiceFeeUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripServiceFeeUnit', function(TripServiceFeeUnit) {
                            return TripServiceFeeUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-service-fee-unit.new', {
            parent: 'trip-service-fee-unit',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-unit-dialog.html',
                    controller: 'TripServiceFeeUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee-unit', null, { reload: 'trip-service-fee-unit' });
                }, function() {
                    $state.go('trip-service-fee-unit');
                });
            }]
        })
        .state('trip-service-fee-unit.edit', {
            parent: 'trip-service-fee-unit',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-unit-dialog.html',
                    controller: 'TripServiceFeeUnitDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripServiceFeeUnit', function(TripServiceFeeUnit) {
                            return TripServiceFeeUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee-unit', null, { reload: 'trip-service-fee-unit' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-service-fee-unit.delete', {
            parent: 'trip-service-fee-unit',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-service-fee-unit/trip-service-fee-unit-delete-dialog.html',
                    controller: 'TripServiceFeeUnitDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripServiceFeeUnit', function(TripServiceFeeUnit) {
                            return TripServiceFeeUnit.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-service-fee-unit', null, { reload: 'trip-service-fee-unit' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
