(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeUnitDeleteController',TripServiceFeeUnitDeleteController);

    TripServiceFeeUnitDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripServiceFeeUnit'];

    function TripServiceFeeUnitDeleteController($uibModalInstance, entity, TripServiceFeeUnit) {
        var vm = this;

        vm.tripServiceFeeUnit = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripServiceFeeUnit.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
