(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripServiceFeeUnitDialogController', TripServiceFeeUnitDialogController);

    TripServiceFeeUnitDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripServiceFeeUnit'];

    function TripServiceFeeUnitDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripServiceFeeUnit) {
        var vm = this;

        vm.tripServiceFeeUnit = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripServiceFeeUnit.id !== null) {
                TripServiceFeeUnit.update(vm.tripServiceFeeUnit, onSaveSuccess, onSaveError);
            } else {
                TripServiceFeeUnit.save(vm.tripServiceFeeUnit, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripServiceFeeUnitUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
