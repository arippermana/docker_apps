(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('RegionDetailController', RegionDetailController);

    RegionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Region', 'City', 'Country'];

    function RegionDetailController($scope, $rootScope, $stateParams, previousState, entity, Region, City, Country) {
        var vm = this;

        vm.region = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:regionUpdate', function(event, result) {
            vm.region = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
