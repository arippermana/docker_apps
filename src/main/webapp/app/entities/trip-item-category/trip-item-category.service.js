(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripItemCategory', TripItemCategory);

    TripItemCategory.$inject = ['$resource'];

    function TripItemCategory ($resource) {
        var resourceUrl =  'api/trip-item-categories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
