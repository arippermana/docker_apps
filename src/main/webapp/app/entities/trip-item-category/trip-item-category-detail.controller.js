(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripItemCategoryDetailController', TripItemCategoryDetailController);

    TripItemCategoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripItemCategory', 'Trip', 'Category'];

    function TripItemCategoryDetailController($scope, $rootScope, $stateParams, previousState, entity, TripItemCategory, Trip, Category) {
        var vm = this;

        vm.tripItemCategory = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripItemCategoryUpdate', function(event, result) {
            vm.tripItemCategory = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
