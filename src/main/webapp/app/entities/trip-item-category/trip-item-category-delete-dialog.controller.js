(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripItemCategoryDeleteController',TripItemCategoryDeleteController);

    TripItemCategoryDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripItemCategory'];

    function TripItemCategoryDeleteController($uibModalInstance, entity, TripItemCategory) {
        var vm = this;

        vm.tripItemCategory = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripItemCategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
