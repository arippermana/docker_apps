(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripItemCategoryController', TripItemCategoryController);

    TripItemCategoryController.$inject = ['TripItemCategory'];

    function TripItemCategoryController(TripItemCategory) {

        var vm = this;

        vm.tripItemCategories = [];

        loadAll();

        function loadAll() {
            TripItemCategory.query(function(result) {
                vm.tripItemCategories = result;
                vm.searchQuery = null;
            });
        }
    }
})();
