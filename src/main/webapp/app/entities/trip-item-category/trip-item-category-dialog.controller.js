(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripItemCategoryDialogController', TripItemCategoryDialogController);

    TripItemCategoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripItemCategory', 'Trip', 'Category'];

    function TripItemCategoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripItemCategory, Trip, Category) {
        var vm = this;

        vm.tripItemCategory = entity;
        vm.clear = clear;
        vm.save = save;
        vm.trips = Trip.query();
        vm.categories = Category.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripItemCategory.id !== null) {
                TripItemCategory.update(vm.tripItemCategory, onSaveSuccess, onSaveError);
            } else {
                TripItemCategory.save(vm.tripItemCategory, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripItemCategoryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
