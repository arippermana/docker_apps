(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-item-category', {
            parent: 'entity',
            url: '/trip-item-category',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripItemCategory.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-item-category/trip-item-categories.html',
                    controller: 'TripItemCategoryController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripItemCategory');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-item-category-detail', {
            parent: 'trip-item-category',
            url: '/trip-item-category/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripItemCategory.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-item-category/trip-item-category-detail.html',
                    controller: 'TripItemCategoryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripItemCategory');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripItemCategory', function($stateParams, TripItemCategory) {
                    return TripItemCategory.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-item-category',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-item-category-detail.edit', {
            parent: 'trip-item-category-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-item-category/trip-item-category-dialog.html',
                    controller: 'TripItemCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripItemCategory', function(TripItemCategory) {
                            return TripItemCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-item-category.new', {
            parent: 'trip-item-category',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-item-category/trip-item-category-dialog.html',
                    controller: 'TripItemCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-item-category', null, { reload: 'trip-item-category' });
                }, function() {
                    $state.go('trip-item-category');
                });
            }]
        })
        .state('trip-item-category.edit', {
            parent: 'trip-item-category',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-item-category/trip-item-category-dialog.html',
                    controller: 'TripItemCategoryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripItemCategory', function(TripItemCategory) {
                            return TripItemCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-item-category', null, { reload: 'trip-item-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-item-category.delete', {
            parent: 'trip-item-category',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-item-category/trip-item-category-delete-dialog.html',
                    controller: 'TripItemCategoryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripItemCategory', function(TripItemCategory) {
                            return TripItemCategory.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-item-category', null, { reload: 'trip-item-category' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
