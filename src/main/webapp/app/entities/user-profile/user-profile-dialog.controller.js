(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserProfileDialogController', UserProfileDialogController);

    UserProfileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserProfile', 'User', 'Gender', 'Country', 'Occupation', 'City', 'Language', 'Currency'];

    function UserProfileDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, UserProfile, User, Gender, Country, Occupation, City, Language, Currency) {
        var vm = this;

        vm.userProfile = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.genders = Gender.query();
        vm.countries = Country.query();
        vm.occupations = Occupation.query();
        vm.cities = City.query();
        vm.languages = Language.query();
        vm.currencies = Currency.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userProfile.id !== null) {
                UserProfile.update(vm.userProfile, onSaveSuccess, onSaveError);
            } else {
                UserProfile.save(vm.userProfile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:userProfileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.birthday = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
