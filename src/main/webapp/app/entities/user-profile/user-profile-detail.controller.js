(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserProfileDetailController', UserProfileDetailController);

    UserProfileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserProfile', 'User', 'Gender', 'Country', 'Occupation', 'City', 'Language', 'Currency'];

    function UserProfileDetailController($scope, $rootScope, $stateParams, previousState, entity, UserProfile, User, Gender, Country, Occupation, City, Language, Currency) {
        var vm = this;

        vm.userProfile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:userProfileUpdate', function(event, result) {
            vm.userProfile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
