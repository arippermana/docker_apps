(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('CurrencyController', CurrencyController);

    CurrencyController.$inject = ['Currency'];

    function CurrencyController(Currency) {

        var vm = this;

        vm.currencies = [];

        loadAll();

        function loadAll() {
            Currency.query(function(result) {
                vm.currencies = result;
                vm.searchQuery = null;
            });
        }
    }
})();
