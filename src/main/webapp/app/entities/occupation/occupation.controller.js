(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('OccupationController', OccupationController);

    OccupationController.$inject = ['Occupation'];

    function OccupationController(Occupation) {

        var vm = this;

        vm.occupations = [];

        loadAll();

        function loadAll() {
            Occupation.query(function(result) {
                vm.occupations = result;
                vm.searchQuery = null;
            });
        }
    }
})();
