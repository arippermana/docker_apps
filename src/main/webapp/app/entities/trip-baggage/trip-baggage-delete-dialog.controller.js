(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripBaggageDeleteController',TripBaggageDeleteController);

    TripBaggageDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripBaggage'];

    function TripBaggageDeleteController($uibModalInstance, entity, TripBaggage) {
        var vm = this;

        vm.tripBaggage = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripBaggage.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
