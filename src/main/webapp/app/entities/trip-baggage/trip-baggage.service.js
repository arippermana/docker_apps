(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripBaggage', TripBaggage);

    TripBaggage.$inject = ['$resource'];

    function TripBaggage ($resource) {
        var resourceUrl =  'api/trip-baggages/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
