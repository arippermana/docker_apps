(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripBaggageDetailController', TripBaggageDetailController);

    TripBaggageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripBaggage'];

    function TripBaggageDetailController($scope, $rootScope, $stateParams, previousState, entity, TripBaggage) {
        var vm = this;

        vm.tripBaggage = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripBaggageUpdate', function(event, result) {
            vm.tripBaggage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
