(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripBaggageController', TripBaggageController);

    TripBaggageController.$inject = ['TripBaggage'];

    function TripBaggageController(TripBaggage) {

        var vm = this;

        vm.tripBaggages = [];

        loadAll();

        function loadAll() {
            TripBaggage.query(function(result) {
                vm.tripBaggages = result;
                vm.searchQuery = null;
            });
        }
    }
})();
