(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-baggage', {
            parent: 'entity',
            url: '/trip-baggage',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripBaggage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-baggage/trip-baggages.html',
                    controller: 'TripBaggageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripBaggage');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-baggage-detail', {
            parent: 'trip-baggage',
            url: '/trip-baggage/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripBaggage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-baggage/trip-baggage-detail.html',
                    controller: 'TripBaggageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripBaggage');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripBaggage', function($stateParams, TripBaggage) {
                    return TripBaggage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-baggage',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-baggage-detail.edit', {
            parent: 'trip-baggage-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-baggage/trip-baggage-dialog.html',
                    controller: 'TripBaggageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripBaggage', function(TripBaggage) {
                            return TripBaggage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-baggage.new', {
            parent: 'trip-baggage',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-baggage/trip-baggage-dialog.html',
                    controller: 'TripBaggageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-baggage', null, { reload: 'trip-baggage' });
                }, function() {
                    $state.go('trip-baggage');
                });
            }]
        })
        .state('trip-baggage.edit', {
            parent: 'trip-baggage',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-baggage/trip-baggage-dialog.html',
                    controller: 'TripBaggageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripBaggage', function(TripBaggage) {
                            return TripBaggage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-baggage', null, { reload: 'trip-baggage' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-baggage.delete', {
            parent: 'trip-baggage',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-baggage/trip-baggage-delete-dialog.html',
                    controller: 'TripBaggageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripBaggage', function(TripBaggage) {
                            return TripBaggage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-baggage', null, { reload: 'trip-baggage' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
