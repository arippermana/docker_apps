(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripBaggageDialogController', TripBaggageDialogController);

    TripBaggageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripBaggage'];

    function TripBaggageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripBaggage) {
        var vm = this;

        vm.tripBaggage = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripBaggage.id !== null) {
                TripBaggage.update(vm.tripBaggage, onSaveSuccess, onSaveError);
            } else {
                TripBaggage.save(vm.tripBaggage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripBaggageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
