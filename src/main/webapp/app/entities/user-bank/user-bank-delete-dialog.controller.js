(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserBankDeleteController',UserBankDeleteController);

    UserBankDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserBank'];

    function UserBankDeleteController($uibModalInstance, entity, UserBank) {
        var vm = this;

        vm.userBank = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserBank.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
