(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('UserBank', UserBank);

    UserBank.$inject = ['$resource'];

    function UserBank ($resource) {
        var resourceUrl =  'api/user-banks/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
