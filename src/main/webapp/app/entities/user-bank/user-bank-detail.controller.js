(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserBankDetailController', UserBankDetailController);

    UserBankDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserBank', 'User'];

    function UserBankDetailController($scope, $rootScope, $stateParams, previousState, entity, UserBank, User) {
        var vm = this;

        vm.userBank = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:userBankUpdate', function(event, result) {
            vm.userBank = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
