(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserBankController', UserBankController);

    UserBankController.$inject = ['UserBank'];

    function UserBankController(UserBank) {

        var vm = this;

        vm.userBanks = [];

        loadAll();

        function loadAll() {
            UserBank.query(function(result) {
                vm.userBanks = result;
                vm.searchQuery = null;
            });
        }
    }
})();
