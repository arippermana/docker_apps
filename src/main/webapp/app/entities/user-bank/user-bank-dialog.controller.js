(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('UserBankDialogController', UserBankDialogController);

    UserBankDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserBank', 'User'];

    function UserBankDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserBank, User) {
        var vm = this;

        vm.userBank = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userBank.id !== null) {
                UserBank.update(vm.userBank, onSaveSuccess, onSaveError);
            } else {
                UserBank.save(vm.userBank, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:userBankUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
