(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-bank', {
            parent: 'entity',
            url: '/user-bank',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.userBank.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-bank/user-banks.html',
                    controller: 'UserBankController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userBank');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-bank-detail', {
            parent: 'user-bank',
            url: '/user-bank/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.userBank.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-bank/user-bank-detail.html',
                    controller: 'UserBankDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userBank');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserBank', function($stateParams, UserBank) {
                    return UserBank.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-bank',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-bank-detail.edit', {
            parent: 'user-bank-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-bank/user-bank-dialog.html',
                    controller: 'UserBankDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserBank', function(UserBank) {
                            return UserBank.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-bank.new', {
            parent: 'user-bank',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-bank/user-bank-dialog.html',
                    controller: 'UserBankDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                bankAccountId: null,
                                bankName: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-bank', null, { reload: 'user-bank' });
                }, function() {
                    $state.go('user-bank');
                });
            }]
        })
        .state('user-bank.edit', {
            parent: 'user-bank',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-bank/user-bank-dialog.html',
                    controller: 'UserBankDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserBank', function(UserBank) {
                            return UserBank.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-bank', null, { reload: 'user-bank' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-bank.delete', {
            parent: 'user-bank',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-bank/user-bank-delete-dialog.html',
                    controller: 'UserBankDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserBank', function(UserBank) {
                            return UserBank.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-bank', null, { reload: 'user-bank' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
