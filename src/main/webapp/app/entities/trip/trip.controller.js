(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripController', TripController);

    TripController.$inject = ['Trip'];

    function TripController(Trip) {

        var vm = this;

        vm.trips = [];

        loadAll();

        function loadAll() {
            Trip.query(function(result) {
                vm.trips = result;
                vm.searchQuery = null;
            });
        }
    }
})();
