(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('Trip', Trip);

    Trip.$inject = ['$resource', 'DateUtils'];

    function Trip ($resource, DateUtils) {
        var resourceUrl =  'api/trips/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.tripDate = DateUtils.convertLocalDateFromServer(data.tripDate);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tripDate = DateUtils.convertLocalDateToServer(copy.tripDate);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.tripDate = DateUtils.convertLocalDateToServer(copy.tripDate);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
