(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripDialogController', TripDialogController);

    TripDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Trip', 'User', 'City', 'TripBaggage', 'TripPaymentMethod', 'UserBank', 'TripServiceFee', 'TripServiceFeeUnit', 'TripStatus'];

    function TripDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Trip, User, City, TripBaggage, TripPaymentMethod, UserBank, TripServiceFee, TripServiceFeeUnit, TripStatus) {
        var vm = this;

        vm.trip = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.cities = City.query();
        vm.tripbaggages = TripBaggage.query();
        vm.trippaymentmethods = TripPaymentMethod.query();
        vm.userbanks = UserBank.query();
        vm.tripservicefees = TripServiceFee.query();
        vm.tripservicefeeunits = TripServiceFeeUnit.query();
        vm.tripstatuses = TripStatus.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.trip.id !== null) {
                Trip.update(vm.trip, onSaveSuccess, onSaveError);
            } else {
                Trip.save(vm.trip, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.tripDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
