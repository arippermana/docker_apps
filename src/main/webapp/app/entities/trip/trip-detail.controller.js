(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripDetailController', TripDetailController);

    TripDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Trip', 'User', 'City', 'TripBaggage', 'TripPaymentMethod', 'UserBank', 'TripServiceFee', 'TripServiceFeeUnit', 'TripStatus'];

    function TripDetailController($scope, $rootScope, $stateParams, previousState, entity, Trip, User, City, TripBaggage, TripPaymentMethod, UserBank, TripServiceFee, TripServiceFeeUnit, TripStatus) {
        var vm = this;

        vm.trip = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripUpdate', function(event, result) {
            vm.trip = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
