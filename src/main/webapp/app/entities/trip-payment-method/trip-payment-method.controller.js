(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripPaymentMethodController', TripPaymentMethodController);

    TripPaymentMethodController.$inject = ['TripPaymentMethod'];

    function TripPaymentMethodController(TripPaymentMethod) {

        var vm = this;

        vm.tripPaymentMethods = [];

        loadAll();

        function loadAll() {
            TripPaymentMethod.query(function(result) {
                vm.tripPaymentMethods = result;
                vm.searchQuery = null;
            });
        }
    }
})();
