(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripPaymentMethodDeleteController',TripPaymentMethodDeleteController);

    TripPaymentMethodDeleteController.$inject = ['$uibModalInstance', 'entity', 'TripPaymentMethod'];

    function TripPaymentMethodDeleteController($uibModalInstance, entity, TripPaymentMethod) {
        var vm = this;

        vm.tripPaymentMethod = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TripPaymentMethod.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
