(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripPaymentMethodDialogController', TripPaymentMethodDialogController);

    TripPaymentMethodDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TripPaymentMethod'];

    function TripPaymentMethodDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TripPaymentMethod) {
        var vm = this;

        vm.tripPaymentMethod = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.tripPaymentMethod.id !== null) {
                TripPaymentMethod.update(vm.tripPaymentMethod, onSaveSuccess, onSaveError);
            } else {
                TripPaymentMethod.save(vm.tripPaymentMethod, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('backpackdaddyApp:tripPaymentMethodUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
