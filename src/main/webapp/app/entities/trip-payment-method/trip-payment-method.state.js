(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('trip-payment-method', {
            parent: 'entity',
            url: '/trip-payment-method',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripPaymentMethod.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-methods.html',
                    controller: 'TripPaymentMethodController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripPaymentMethod');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('trip-payment-method-detail', {
            parent: 'trip-payment-method',
            url: '/trip-payment-method/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'backpackdaddyApp.tripPaymentMethod.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-method-detail.html',
                    controller: 'TripPaymentMethodDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('tripPaymentMethod');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'TripPaymentMethod', function($stateParams, TripPaymentMethod) {
                    return TripPaymentMethod.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'trip-payment-method',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('trip-payment-method-detail.edit', {
            parent: 'trip-payment-method-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-method-dialog.html',
                    controller: 'TripPaymentMethodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripPaymentMethod', function(TripPaymentMethod) {
                            return TripPaymentMethod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-payment-method.new', {
            parent: 'trip-payment-method',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-method-dialog.html',
                    controller: 'TripPaymentMethodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('trip-payment-method', null, { reload: 'trip-payment-method' });
                }, function() {
                    $state.go('trip-payment-method');
                });
            }]
        })
        .state('trip-payment-method.edit', {
            parent: 'trip-payment-method',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-method-dialog.html',
                    controller: 'TripPaymentMethodDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['TripPaymentMethod', function(TripPaymentMethod) {
                            return TripPaymentMethod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-payment-method', null, { reload: 'trip-payment-method' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('trip-payment-method.delete', {
            parent: 'trip-payment-method',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/trip-payment-method/trip-payment-method-delete-dialog.html',
                    controller: 'TripPaymentMethodDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['TripPaymentMethod', function(TripPaymentMethod) {
                            return TripPaymentMethod.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('trip-payment-method', null, { reload: 'trip-payment-method' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
