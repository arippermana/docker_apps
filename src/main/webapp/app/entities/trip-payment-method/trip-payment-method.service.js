(function() {
    'use strict';
    angular
        .module('backpackdaddyApp')
        .factory('TripPaymentMethod', TripPaymentMethod);

    TripPaymentMethod.$inject = ['$resource'];

    function TripPaymentMethod ($resource) {
        var resourceUrl =  'api/trip-payment-methods/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
