(function() {
    'use strict';

    angular
        .module('backpackdaddyApp')
        .controller('TripPaymentMethodDetailController', TripPaymentMethodDetailController);

    TripPaymentMethodDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TripPaymentMethod'];

    function TripPaymentMethodDetailController($scope, $rootScope, $stateParams, previousState, entity, TripPaymentMethod) {
        var vm = this;

        vm.tripPaymentMethod = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('backpackdaddyApp:tripPaymentMethodUpdate', function(event, result) {
            vm.tripPaymentMethod = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
