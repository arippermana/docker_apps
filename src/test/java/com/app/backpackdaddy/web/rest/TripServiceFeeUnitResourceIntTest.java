package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripServiceFeeUnit;
import com.app.backpackdaddy.repository.TripServiceFeeUnitRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripServiceFeeUnitResource REST controller.
 *
 * @see TripServiceFeeUnitResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripServiceFeeUnitResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TripServiceFeeUnitRepository tripServiceFeeUnitRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripServiceFeeUnitMockMvc;

    private TripServiceFeeUnit tripServiceFeeUnit;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripServiceFeeUnitResource tripServiceFeeUnitResource = new TripServiceFeeUnitResource(tripServiceFeeUnitRepository);
        this.restTripServiceFeeUnitMockMvc = MockMvcBuilders.standaloneSetup(tripServiceFeeUnitResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripServiceFeeUnit createEntity(EntityManager em) {
        TripServiceFeeUnit tripServiceFeeUnit = new TripServiceFeeUnit()
            .name(DEFAULT_NAME);
        return tripServiceFeeUnit;
    }

    @Before
    public void initTest() {
        tripServiceFeeUnit = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripServiceFeeUnit() throws Exception {
        int databaseSizeBeforeCreate = tripServiceFeeUnitRepository.findAll().size();

        // Create the TripServiceFeeUnit
        restTripServiceFeeUnitMockMvc.perform(post("/api/trip-service-fee-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFeeUnit)))
            .andExpect(status().isCreated());

        // Validate the TripServiceFeeUnit in the database
        List<TripServiceFeeUnit> tripServiceFeeUnitList = tripServiceFeeUnitRepository.findAll();
        assertThat(tripServiceFeeUnitList).hasSize(databaseSizeBeforeCreate + 1);
        TripServiceFeeUnit testTripServiceFeeUnit = tripServiceFeeUnitList.get(tripServiceFeeUnitList.size() - 1);
        assertThat(testTripServiceFeeUnit.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTripServiceFeeUnitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripServiceFeeUnitRepository.findAll().size();

        // Create the TripServiceFeeUnit with an existing ID
        tripServiceFeeUnit.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripServiceFeeUnitMockMvc.perform(post("/api/trip-service-fee-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFeeUnit)))
            .andExpect(status().isBadRequest());

        // Validate the TripServiceFeeUnit in the database
        List<TripServiceFeeUnit> tripServiceFeeUnitList = tripServiceFeeUnitRepository.findAll();
        assertThat(tripServiceFeeUnitList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripServiceFeeUnits() throws Exception {
        // Initialize the database
        tripServiceFeeUnitRepository.saveAndFlush(tripServiceFeeUnit);

        // Get all the tripServiceFeeUnitList
        restTripServiceFeeUnitMockMvc.perform(get("/api/trip-service-fee-units?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripServiceFeeUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTripServiceFeeUnit() throws Exception {
        // Initialize the database
        tripServiceFeeUnitRepository.saveAndFlush(tripServiceFeeUnit);

        // Get the tripServiceFeeUnit
        restTripServiceFeeUnitMockMvc.perform(get("/api/trip-service-fee-units/{id}", tripServiceFeeUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripServiceFeeUnit.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripServiceFeeUnit() throws Exception {
        // Get the tripServiceFeeUnit
        restTripServiceFeeUnitMockMvc.perform(get("/api/trip-service-fee-units/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripServiceFeeUnit() throws Exception {
        // Initialize the database
        tripServiceFeeUnitRepository.saveAndFlush(tripServiceFeeUnit);
        int databaseSizeBeforeUpdate = tripServiceFeeUnitRepository.findAll().size();

        // Update the tripServiceFeeUnit
        TripServiceFeeUnit updatedTripServiceFeeUnit = tripServiceFeeUnitRepository.findOne(tripServiceFeeUnit.getId());
        // Disconnect from session so that the updates on updatedTripServiceFeeUnit are not directly saved in db
        em.detach(updatedTripServiceFeeUnit);
        updatedTripServiceFeeUnit
            .name(UPDATED_NAME);

        restTripServiceFeeUnitMockMvc.perform(put("/api/trip-service-fee-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripServiceFeeUnit)))
            .andExpect(status().isOk());

        // Validate the TripServiceFeeUnit in the database
        List<TripServiceFeeUnit> tripServiceFeeUnitList = tripServiceFeeUnitRepository.findAll();
        assertThat(tripServiceFeeUnitList).hasSize(databaseSizeBeforeUpdate);
        TripServiceFeeUnit testTripServiceFeeUnit = tripServiceFeeUnitList.get(tripServiceFeeUnitList.size() - 1);
        assertThat(testTripServiceFeeUnit.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTripServiceFeeUnit() throws Exception {
        int databaseSizeBeforeUpdate = tripServiceFeeUnitRepository.findAll().size();

        // Create the TripServiceFeeUnit

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripServiceFeeUnitMockMvc.perform(put("/api/trip-service-fee-units")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFeeUnit)))
            .andExpect(status().isCreated());

        // Validate the TripServiceFeeUnit in the database
        List<TripServiceFeeUnit> tripServiceFeeUnitList = tripServiceFeeUnitRepository.findAll();
        assertThat(tripServiceFeeUnitList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripServiceFeeUnit() throws Exception {
        // Initialize the database
        tripServiceFeeUnitRepository.saveAndFlush(tripServiceFeeUnit);
        int databaseSizeBeforeDelete = tripServiceFeeUnitRepository.findAll().size();

        // Get the tripServiceFeeUnit
        restTripServiceFeeUnitMockMvc.perform(delete("/api/trip-service-fee-units/{id}", tripServiceFeeUnit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripServiceFeeUnit> tripServiceFeeUnitList = tripServiceFeeUnitRepository.findAll();
        assertThat(tripServiceFeeUnitList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripServiceFeeUnit.class);
        TripServiceFeeUnit tripServiceFeeUnit1 = new TripServiceFeeUnit();
        tripServiceFeeUnit1.setId(1L);
        TripServiceFeeUnit tripServiceFeeUnit2 = new TripServiceFeeUnit();
        tripServiceFeeUnit2.setId(tripServiceFeeUnit1.getId());
        assertThat(tripServiceFeeUnit1).isEqualTo(tripServiceFeeUnit2);
        tripServiceFeeUnit2.setId(2L);
        assertThat(tripServiceFeeUnit1).isNotEqualTo(tripServiceFeeUnit2);
        tripServiceFeeUnit1.setId(null);
        assertThat(tripServiceFeeUnit1).isNotEqualTo(tripServiceFeeUnit2);
    }
}
