package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripServiceFee;
import com.app.backpackdaddy.repository.TripServiceFeeRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripServiceFeeResource REST controller.
 *
 * @see TripServiceFeeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripServiceFeeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TripServiceFeeRepository tripServiceFeeRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripServiceFeeMockMvc;

    private TripServiceFee tripServiceFee;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripServiceFeeResource tripServiceFeeResource = new TripServiceFeeResource(tripServiceFeeRepository);
        this.restTripServiceFeeMockMvc = MockMvcBuilders.standaloneSetup(tripServiceFeeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripServiceFee createEntity(EntityManager em) {
        TripServiceFee tripServiceFee = new TripServiceFee()
            .name(DEFAULT_NAME);
        return tripServiceFee;
    }

    @Before
    public void initTest() {
        tripServiceFee = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripServiceFee() throws Exception {
        int databaseSizeBeforeCreate = tripServiceFeeRepository.findAll().size();

        // Create the TripServiceFee
        restTripServiceFeeMockMvc.perform(post("/api/trip-service-fees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFee)))
            .andExpect(status().isCreated());

        // Validate the TripServiceFee in the database
        List<TripServiceFee> tripServiceFeeList = tripServiceFeeRepository.findAll();
        assertThat(tripServiceFeeList).hasSize(databaseSizeBeforeCreate + 1);
        TripServiceFee testTripServiceFee = tripServiceFeeList.get(tripServiceFeeList.size() - 1);
        assertThat(testTripServiceFee.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTripServiceFeeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripServiceFeeRepository.findAll().size();

        // Create the TripServiceFee with an existing ID
        tripServiceFee.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripServiceFeeMockMvc.perform(post("/api/trip-service-fees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFee)))
            .andExpect(status().isBadRequest());

        // Validate the TripServiceFee in the database
        List<TripServiceFee> tripServiceFeeList = tripServiceFeeRepository.findAll();
        assertThat(tripServiceFeeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripServiceFees() throws Exception {
        // Initialize the database
        tripServiceFeeRepository.saveAndFlush(tripServiceFee);

        // Get all the tripServiceFeeList
        restTripServiceFeeMockMvc.perform(get("/api/trip-service-fees?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripServiceFee.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTripServiceFee() throws Exception {
        // Initialize the database
        tripServiceFeeRepository.saveAndFlush(tripServiceFee);

        // Get the tripServiceFee
        restTripServiceFeeMockMvc.perform(get("/api/trip-service-fees/{id}", tripServiceFee.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripServiceFee.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripServiceFee() throws Exception {
        // Get the tripServiceFee
        restTripServiceFeeMockMvc.perform(get("/api/trip-service-fees/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripServiceFee() throws Exception {
        // Initialize the database
        tripServiceFeeRepository.saveAndFlush(tripServiceFee);
        int databaseSizeBeforeUpdate = tripServiceFeeRepository.findAll().size();

        // Update the tripServiceFee
        TripServiceFee updatedTripServiceFee = tripServiceFeeRepository.findOne(tripServiceFee.getId());
        // Disconnect from session so that the updates on updatedTripServiceFee are not directly saved in db
        em.detach(updatedTripServiceFee);
        updatedTripServiceFee
            .name(UPDATED_NAME);

        restTripServiceFeeMockMvc.perform(put("/api/trip-service-fees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripServiceFee)))
            .andExpect(status().isOk());

        // Validate the TripServiceFee in the database
        List<TripServiceFee> tripServiceFeeList = tripServiceFeeRepository.findAll();
        assertThat(tripServiceFeeList).hasSize(databaseSizeBeforeUpdate);
        TripServiceFee testTripServiceFee = tripServiceFeeList.get(tripServiceFeeList.size() - 1);
        assertThat(testTripServiceFee.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTripServiceFee() throws Exception {
        int databaseSizeBeforeUpdate = tripServiceFeeRepository.findAll().size();

        // Create the TripServiceFee

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripServiceFeeMockMvc.perform(put("/api/trip-service-fees")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripServiceFee)))
            .andExpect(status().isCreated());

        // Validate the TripServiceFee in the database
        List<TripServiceFee> tripServiceFeeList = tripServiceFeeRepository.findAll();
        assertThat(tripServiceFeeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripServiceFee() throws Exception {
        // Initialize the database
        tripServiceFeeRepository.saveAndFlush(tripServiceFee);
        int databaseSizeBeforeDelete = tripServiceFeeRepository.findAll().size();

        // Get the tripServiceFee
        restTripServiceFeeMockMvc.perform(delete("/api/trip-service-fees/{id}", tripServiceFee.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripServiceFee> tripServiceFeeList = tripServiceFeeRepository.findAll();
        assertThat(tripServiceFeeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripServiceFee.class);
        TripServiceFee tripServiceFee1 = new TripServiceFee();
        tripServiceFee1.setId(1L);
        TripServiceFee tripServiceFee2 = new TripServiceFee();
        tripServiceFee2.setId(tripServiceFee1.getId());
        assertThat(tripServiceFee1).isEqualTo(tripServiceFee2);
        tripServiceFee2.setId(2L);
        assertThat(tripServiceFee1).isNotEqualTo(tripServiceFee2);
        tripServiceFee1.setId(null);
        assertThat(tripServiceFee1).isNotEqualTo(tripServiceFee2);
    }
}
