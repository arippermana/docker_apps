package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripPaymentMethod;
import com.app.backpackdaddy.repository.TripPaymentMethodRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripPaymentMethodResource REST controller.
 *
 * @see TripPaymentMethodResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripPaymentMethodResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TripPaymentMethodRepository tripPaymentMethodRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripPaymentMethodMockMvc;

    private TripPaymentMethod tripPaymentMethod;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripPaymentMethodResource tripPaymentMethodResource = new TripPaymentMethodResource(tripPaymentMethodRepository);
        this.restTripPaymentMethodMockMvc = MockMvcBuilders.standaloneSetup(tripPaymentMethodResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripPaymentMethod createEntity(EntityManager em) {
        TripPaymentMethod tripPaymentMethod = new TripPaymentMethod()
            .name(DEFAULT_NAME);
        return tripPaymentMethod;
    }

    @Before
    public void initTest() {
        tripPaymentMethod = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripPaymentMethod() throws Exception {
        int databaseSizeBeforeCreate = tripPaymentMethodRepository.findAll().size();

        // Create the TripPaymentMethod
        restTripPaymentMethodMockMvc.perform(post("/api/trip-payment-methods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripPaymentMethod)))
            .andExpect(status().isCreated());

        // Validate the TripPaymentMethod in the database
        List<TripPaymentMethod> tripPaymentMethodList = tripPaymentMethodRepository.findAll();
        assertThat(tripPaymentMethodList).hasSize(databaseSizeBeforeCreate + 1);
        TripPaymentMethod testTripPaymentMethod = tripPaymentMethodList.get(tripPaymentMethodList.size() - 1);
        assertThat(testTripPaymentMethod.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTripPaymentMethodWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripPaymentMethodRepository.findAll().size();

        // Create the TripPaymentMethod with an existing ID
        tripPaymentMethod.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripPaymentMethodMockMvc.perform(post("/api/trip-payment-methods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripPaymentMethod)))
            .andExpect(status().isBadRequest());

        // Validate the TripPaymentMethod in the database
        List<TripPaymentMethod> tripPaymentMethodList = tripPaymentMethodRepository.findAll();
        assertThat(tripPaymentMethodList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripPaymentMethods() throws Exception {
        // Initialize the database
        tripPaymentMethodRepository.saveAndFlush(tripPaymentMethod);

        // Get all the tripPaymentMethodList
        restTripPaymentMethodMockMvc.perform(get("/api/trip-payment-methods?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripPaymentMethod.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTripPaymentMethod() throws Exception {
        // Initialize the database
        tripPaymentMethodRepository.saveAndFlush(tripPaymentMethod);

        // Get the tripPaymentMethod
        restTripPaymentMethodMockMvc.perform(get("/api/trip-payment-methods/{id}", tripPaymentMethod.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripPaymentMethod.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripPaymentMethod() throws Exception {
        // Get the tripPaymentMethod
        restTripPaymentMethodMockMvc.perform(get("/api/trip-payment-methods/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripPaymentMethod() throws Exception {
        // Initialize the database
        tripPaymentMethodRepository.saveAndFlush(tripPaymentMethod);
        int databaseSizeBeforeUpdate = tripPaymentMethodRepository.findAll().size();

        // Update the tripPaymentMethod
        TripPaymentMethod updatedTripPaymentMethod = tripPaymentMethodRepository.findOne(tripPaymentMethod.getId());
        // Disconnect from session so that the updates on updatedTripPaymentMethod are not directly saved in db
        em.detach(updatedTripPaymentMethod);
        updatedTripPaymentMethod
            .name(UPDATED_NAME);

        restTripPaymentMethodMockMvc.perform(put("/api/trip-payment-methods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripPaymentMethod)))
            .andExpect(status().isOk());

        // Validate the TripPaymentMethod in the database
        List<TripPaymentMethod> tripPaymentMethodList = tripPaymentMethodRepository.findAll();
        assertThat(tripPaymentMethodList).hasSize(databaseSizeBeforeUpdate);
        TripPaymentMethod testTripPaymentMethod = tripPaymentMethodList.get(tripPaymentMethodList.size() - 1);
        assertThat(testTripPaymentMethod.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTripPaymentMethod() throws Exception {
        int databaseSizeBeforeUpdate = tripPaymentMethodRepository.findAll().size();

        // Create the TripPaymentMethod

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripPaymentMethodMockMvc.perform(put("/api/trip-payment-methods")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripPaymentMethod)))
            .andExpect(status().isCreated());

        // Validate the TripPaymentMethod in the database
        List<TripPaymentMethod> tripPaymentMethodList = tripPaymentMethodRepository.findAll();
        assertThat(tripPaymentMethodList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripPaymentMethod() throws Exception {
        // Initialize the database
        tripPaymentMethodRepository.saveAndFlush(tripPaymentMethod);
        int databaseSizeBeforeDelete = tripPaymentMethodRepository.findAll().size();

        // Get the tripPaymentMethod
        restTripPaymentMethodMockMvc.perform(delete("/api/trip-payment-methods/{id}", tripPaymentMethod.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripPaymentMethod> tripPaymentMethodList = tripPaymentMethodRepository.findAll();
        assertThat(tripPaymentMethodList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripPaymentMethod.class);
        TripPaymentMethod tripPaymentMethod1 = new TripPaymentMethod();
        tripPaymentMethod1.setId(1L);
        TripPaymentMethod tripPaymentMethod2 = new TripPaymentMethod();
        tripPaymentMethod2.setId(tripPaymentMethod1.getId());
        assertThat(tripPaymentMethod1).isEqualTo(tripPaymentMethod2);
        tripPaymentMethod2.setId(2L);
        assertThat(tripPaymentMethod1).isNotEqualTo(tripPaymentMethod2);
        tripPaymentMethod1.setId(null);
        assertThat(tripPaymentMethod1).isNotEqualTo(tripPaymentMethod2);
    }
}
