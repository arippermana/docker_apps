package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripBaggage;
import com.app.backpackdaddy.repository.TripBaggageRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripBaggageResource REST controller.
 *
 * @see TripBaggageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripBaggageResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TripBaggageRepository tripBaggageRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripBaggageMockMvc;

    private TripBaggage tripBaggage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripBaggageResource tripBaggageResource = new TripBaggageResource(tripBaggageRepository);
        this.restTripBaggageMockMvc = MockMvcBuilders.standaloneSetup(tripBaggageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripBaggage createEntity(EntityManager em) {
        TripBaggage tripBaggage = new TripBaggage()
            .name(DEFAULT_NAME);
        return tripBaggage;
    }

    @Before
    public void initTest() {
        tripBaggage = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripBaggage() throws Exception {
        int databaseSizeBeforeCreate = tripBaggageRepository.findAll().size();

        // Create the TripBaggage
        restTripBaggageMockMvc.perform(post("/api/trip-baggages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripBaggage)))
            .andExpect(status().isCreated());

        // Validate the TripBaggage in the database
        List<TripBaggage> tripBaggageList = tripBaggageRepository.findAll();
        assertThat(tripBaggageList).hasSize(databaseSizeBeforeCreate + 1);
        TripBaggage testTripBaggage = tripBaggageList.get(tripBaggageList.size() - 1);
        assertThat(testTripBaggage.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTripBaggageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripBaggageRepository.findAll().size();

        // Create the TripBaggage with an existing ID
        tripBaggage.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripBaggageMockMvc.perform(post("/api/trip-baggages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripBaggage)))
            .andExpect(status().isBadRequest());

        // Validate the TripBaggage in the database
        List<TripBaggage> tripBaggageList = tripBaggageRepository.findAll();
        assertThat(tripBaggageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripBaggages() throws Exception {
        // Initialize the database
        tripBaggageRepository.saveAndFlush(tripBaggage);

        // Get all the tripBaggageList
        restTripBaggageMockMvc.perform(get("/api/trip-baggages?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripBaggage.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTripBaggage() throws Exception {
        // Initialize the database
        tripBaggageRepository.saveAndFlush(tripBaggage);

        // Get the tripBaggage
        restTripBaggageMockMvc.perform(get("/api/trip-baggages/{id}", tripBaggage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripBaggage.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripBaggage() throws Exception {
        // Get the tripBaggage
        restTripBaggageMockMvc.perform(get("/api/trip-baggages/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripBaggage() throws Exception {
        // Initialize the database
        tripBaggageRepository.saveAndFlush(tripBaggage);
        int databaseSizeBeforeUpdate = tripBaggageRepository.findAll().size();

        // Update the tripBaggage
        TripBaggage updatedTripBaggage = tripBaggageRepository.findOne(tripBaggage.getId());
        // Disconnect from session so that the updates on updatedTripBaggage are not directly saved in db
        em.detach(updatedTripBaggage);
        updatedTripBaggage
            .name(UPDATED_NAME);

        restTripBaggageMockMvc.perform(put("/api/trip-baggages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripBaggage)))
            .andExpect(status().isOk());

        // Validate the TripBaggage in the database
        List<TripBaggage> tripBaggageList = tripBaggageRepository.findAll();
        assertThat(tripBaggageList).hasSize(databaseSizeBeforeUpdate);
        TripBaggage testTripBaggage = tripBaggageList.get(tripBaggageList.size() - 1);
        assertThat(testTripBaggage.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTripBaggage() throws Exception {
        int databaseSizeBeforeUpdate = tripBaggageRepository.findAll().size();

        // Create the TripBaggage

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripBaggageMockMvc.perform(put("/api/trip-baggages")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripBaggage)))
            .andExpect(status().isCreated());

        // Validate the TripBaggage in the database
        List<TripBaggage> tripBaggageList = tripBaggageRepository.findAll();
        assertThat(tripBaggageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripBaggage() throws Exception {
        // Initialize the database
        tripBaggageRepository.saveAndFlush(tripBaggage);
        int databaseSizeBeforeDelete = tripBaggageRepository.findAll().size();

        // Get the tripBaggage
        restTripBaggageMockMvc.perform(delete("/api/trip-baggages/{id}", tripBaggage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripBaggage> tripBaggageList = tripBaggageRepository.findAll();
        assertThat(tripBaggageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripBaggage.class);
        TripBaggage tripBaggage1 = new TripBaggage();
        tripBaggage1.setId(1L);
        TripBaggage tripBaggage2 = new TripBaggage();
        tripBaggage2.setId(tripBaggage1.getId());
        assertThat(tripBaggage1).isEqualTo(tripBaggage2);
        tripBaggage2.setId(2L);
        assertThat(tripBaggage1).isNotEqualTo(tripBaggage2);
        tripBaggage1.setId(null);
        assertThat(tripBaggage1).isNotEqualTo(tripBaggage2);
    }
}
