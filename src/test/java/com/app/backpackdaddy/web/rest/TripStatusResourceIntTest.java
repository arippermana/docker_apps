package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripStatus;
import com.app.backpackdaddy.repository.TripStatusRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripStatusResource REST controller.
 *
 * @see TripStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripStatusResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private TripStatusRepository tripStatusRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripStatusMockMvc;

    private TripStatus tripStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripStatusResource tripStatusResource = new TripStatusResource(tripStatusRepository);
        this.restTripStatusMockMvc = MockMvcBuilders.standaloneSetup(tripStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripStatus createEntity(EntityManager em) {
        TripStatus tripStatus = new TripStatus()
            .name(DEFAULT_NAME);
        return tripStatus;
    }

    @Before
    public void initTest() {
        tripStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripStatus() throws Exception {
        int databaseSizeBeforeCreate = tripStatusRepository.findAll().size();

        // Create the TripStatus
        restTripStatusMockMvc.perform(post("/api/trip-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripStatus)))
            .andExpect(status().isCreated());

        // Validate the TripStatus in the database
        List<TripStatus> tripStatusList = tripStatusRepository.findAll();
        assertThat(tripStatusList).hasSize(databaseSizeBeforeCreate + 1);
        TripStatus testTripStatus = tripStatusList.get(tripStatusList.size() - 1);
        assertThat(testTripStatus.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createTripStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripStatusRepository.findAll().size();

        // Create the TripStatus with an existing ID
        tripStatus.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripStatusMockMvc.perform(post("/api/trip-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripStatus)))
            .andExpect(status().isBadRequest());

        // Validate the TripStatus in the database
        List<TripStatus> tripStatusList = tripStatusRepository.findAll();
        assertThat(tripStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripStatuses() throws Exception {
        // Initialize the database
        tripStatusRepository.saveAndFlush(tripStatus);

        // Get all the tripStatusList
        restTripStatusMockMvc.perform(get("/api/trip-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getTripStatus() throws Exception {
        // Initialize the database
        tripStatusRepository.saveAndFlush(tripStatus);

        // Get the tripStatus
        restTripStatusMockMvc.perform(get("/api/trip-statuses/{id}", tripStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTripStatus() throws Exception {
        // Get the tripStatus
        restTripStatusMockMvc.perform(get("/api/trip-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripStatus() throws Exception {
        // Initialize the database
        tripStatusRepository.saveAndFlush(tripStatus);
        int databaseSizeBeforeUpdate = tripStatusRepository.findAll().size();

        // Update the tripStatus
        TripStatus updatedTripStatus = tripStatusRepository.findOne(tripStatus.getId());
        // Disconnect from session so that the updates on updatedTripStatus are not directly saved in db
        em.detach(updatedTripStatus);
        updatedTripStatus
            .name(UPDATED_NAME);

        restTripStatusMockMvc.perform(put("/api/trip-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripStatus)))
            .andExpect(status().isOk());

        // Validate the TripStatus in the database
        List<TripStatus> tripStatusList = tripStatusRepository.findAll();
        assertThat(tripStatusList).hasSize(databaseSizeBeforeUpdate);
        TripStatus testTripStatus = tripStatusList.get(tripStatusList.size() - 1);
        assertThat(testTripStatus.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTripStatus() throws Exception {
        int databaseSizeBeforeUpdate = tripStatusRepository.findAll().size();

        // Create the TripStatus

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripStatusMockMvc.perform(put("/api/trip-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripStatus)))
            .andExpect(status().isCreated());

        // Validate the TripStatus in the database
        List<TripStatus> tripStatusList = tripStatusRepository.findAll();
        assertThat(tripStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripStatus() throws Exception {
        // Initialize the database
        tripStatusRepository.saveAndFlush(tripStatus);
        int databaseSizeBeforeDelete = tripStatusRepository.findAll().size();

        // Get the tripStatus
        restTripStatusMockMvc.perform(delete("/api/trip-statuses/{id}", tripStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripStatus> tripStatusList = tripStatusRepository.findAll();
        assertThat(tripStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripStatus.class);
        TripStatus tripStatus1 = new TripStatus();
        tripStatus1.setId(1L);
        TripStatus tripStatus2 = new TripStatus();
        tripStatus2.setId(tripStatus1.getId());
        assertThat(tripStatus1).isEqualTo(tripStatus2);
        tripStatus2.setId(2L);
        assertThat(tripStatus1).isNotEqualTo(tripStatus2);
        tripStatus1.setId(null);
        assertThat(tripStatus1).isNotEqualTo(tripStatus2);
    }
}
