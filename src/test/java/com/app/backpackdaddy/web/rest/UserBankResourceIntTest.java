package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.UserBank;
import com.app.backpackdaddy.repository.UserBankRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserBankResource REST controller.
 *
 * @see UserBankResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class UserBankResourceIntTest {

    private static final String DEFAULT_BANK_ACCOUNT_ID = "AAAAAAAAAA";
    private static final String UPDATED_BANK_ACCOUNT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_BANK_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BANK_NAME = "BBBBBBBBBB";

    @Autowired
    private UserBankRepository userBankRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserBankMockMvc;

    private UserBank userBank;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserBankResource userBankResource = new UserBankResource(userBankRepository);
        this.restUserBankMockMvc = MockMvcBuilders.standaloneSetup(userBankResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserBank createEntity(EntityManager em) {
        UserBank userBank = new UserBank()
            .bankAccountId(DEFAULT_BANK_ACCOUNT_ID)
            .bankName(DEFAULT_BANK_NAME);
        return userBank;
    }

    @Before
    public void initTest() {
        userBank = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserBank() throws Exception {
        int databaseSizeBeforeCreate = userBankRepository.findAll().size();

        // Create the UserBank
        restUserBankMockMvc.perform(post("/api/user-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userBank)))
            .andExpect(status().isCreated());

        // Validate the UserBank in the database
        List<UserBank> userBankList = userBankRepository.findAll();
        assertThat(userBankList).hasSize(databaseSizeBeforeCreate + 1);
        UserBank testUserBank = userBankList.get(userBankList.size() - 1);
        assertThat(testUserBank.getBankAccountId()).isEqualTo(DEFAULT_BANK_ACCOUNT_ID);
        assertThat(testUserBank.getBankName()).isEqualTo(DEFAULT_BANK_NAME);
    }

    @Test
    @Transactional
    public void createUserBankWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userBankRepository.findAll().size();

        // Create the UserBank with an existing ID
        userBank.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserBankMockMvc.perform(post("/api/user-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userBank)))
            .andExpect(status().isBadRequest());

        // Validate the UserBank in the database
        List<UserBank> userBankList = userBankRepository.findAll();
        assertThat(userBankList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllUserBanks() throws Exception {
        // Initialize the database
        userBankRepository.saveAndFlush(userBank);

        // Get all the userBankList
        restUserBankMockMvc.perform(get("/api/user-banks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userBank.getId().intValue())))
            .andExpect(jsonPath("$.[*].bankAccountId").value(hasItem(DEFAULT_BANK_ACCOUNT_ID.toString())))
            .andExpect(jsonPath("$.[*].bankName").value(hasItem(DEFAULT_BANK_NAME.toString())));
    }

    @Test
    @Transactional
    public void getUserBank() throws Exception {
        // Initialize the database
        userBankRepository.saveAndFlush(userBank);

        // Get the userBank
        restUserBankMockMvc.perform(get("/api/user-banks/{id}", userBank.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userBank.getId().intValue()))
            .andExpect(jsonPath("$.bankAccountId").value(DEFAULT_BANK_ACCOUNT_ID.toString()))
            .andExpect(jsonPath("$.bankName").value(DEFAULT_BANK_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserBank() throws Exception {
        // Get the userBank
        restUserBankMockMvc.perform(get("/api/user-banks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserBank() throws Exception {
        // Initialize the database
        userBankRepository.saveAndFlush(userBank);
        int databaseSizeBeforeUpdate = userBankRepository.findAll().size();

        // Update the userBank
        UserBank updatedUserBank = userBankRepository.findOne(userBank.getId());
        // Disconnect from session so that the updates on updatedUserBank are not directly saved in db
        em.detach(updatedUserBank);
        updatedUserBank
            .bankAccountId(UPDATED_BANK_ACCOUNT_ID)
            .bankName(UPDATED_BANK_NAME);

        restUserBankMockMvc.perform(put("/api/user-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserBank)))
            .andExpect(status().isOk());

        // Validate the UserBank in the database
        List<UserBank> userBankList = userBankRepository.findAll();
        assertThat(userBankList).hasSize(databaseSizeBeforeUpdate);
        UserBank testUserBank = userBankList.get(userBankList.size() - 1);
        assertThat(testUserBank.getBankAccountId()).isEqualTo(UPDATED_BANK_ACCOUNT_ID);
        assertThat(testUserBank.getBankName()).isEqualTo(UPDATED_BANK_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingUserBank() throws Exception {
        int databaseSizeBeforeUpdate = userBankRepository.findAll().size();

        // Create the UserBank

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserBankMockMvc.perform(put("/api/user-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userBank)))
            .andExpect(status().isCreated());

        // Validate the UserBank in the database
        List<UserBank> userBankList = userBankRepository.findAll();
        assertThat(userBankList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserBank() throws Exception {
        // Initialize the database
        userBankRepository.saveAndFlush(userBank);
        int databaseSizeBeforeDelete = userBankRepository.findAll().size();

        // Get the userBank
        restUserBankMockMvc.perform(delete("/api/user-banks/{id}", userBank.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserBank> userBankList = userBankRepository.findAll();
        assertThat(userBankList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserBank.class);
        UserBank userBank1 = new UserBank();
        userBank1.setId(1L);
        UserBank userBank2 = new UserBank();
        userBank2.setId(userBank1.getId());
        assertThat(userBank1).isEqualTo(userBank2);
        userBank2.setId(2L);
        assertThat(userBank1).isNotEqualTo(userBank2);
        userBank1.setId(null);
        assertThat(userBank1).isNotEqualTo(userBank2);
    }
}
