package com.app.backpackdaddy.web.rest;

import com.app.backpackdaddy.BackpackdaddyApp;

import com.app.backpackdaddy.domain.TripItemCategory;
import com.app.backpackdaddy.repository.TripItemCategoryRepository;
import com.app.backpackdaddy.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.app.backpackdaddy.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TripItemCategoryResource REST controller.
 *
 * @see TripItemCategoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackpackdaddyApp.class)
public class TripItemCategoryResourceIntTest {

    @Autowired
    private TripItemCategoryRepository tripItemCategoryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTripItemCategoryMockMvc;

    private TripItemCategory tripItemCategory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TripItemCategoryResource tripItemCategoryResource = new TripItemCategoryResource(tripItemCategoryRepository);
        this.restTripItemCategoryMockMvc = MockMvcBuilders.standaloneSetup(tripItemCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TripItemCategory createEntity(EntityManager em) {
        TripItemCategory tripItemCategory = new TripItemCategory();
        return tripItemCategory;
    }

    @Before
    public void initTest() {
        tripItemCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createTripItemCategory() throws Exception {
        int databaseSizeBeforeCreate = tripItemCategoryRepository.findAll().size();

        // Create the TripItemCategory
        restTripItemCategoryMockMvc.perform(post("/api/trip-item-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripItemCategory)))
            .andExpect(status().isCreated());

        // Validate the TripItemCategory in the database
        List<TripItemCategory> tripItemCategoryList = tripItemCategoryRepository.findAll();
        assertThat(tripItemCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        TripItemCategory testTripItemCategory = tripItemCategoryList.get(tripItemCategoryList.size() - 1);
    }

    @Test
    @Transactional
    public void createTripItemCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tripItemCategoryRepository.findAll().size();

        // Create the TripItemCategory with an existing ID
        tripItemCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTripItemCategoryMockMvc.perform(post("/api/trip-item-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripItemCategory)))
            .andExpect(status().isBadRequest());

        // Validate the TripItemCategory in the database
        List<TripItemCategory> tripItemCategoryList = tripItemCategoryRepository.findAll();
        assertThat(tripItemCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTripItemCategories() throws Exception {
        // Initialize the database
        tripItemCategoryRepository.saveAndFlush(tripItemCategory);

        // Get all the tripItemCategoryList
        restTripItemCategoryMockMvc.perform(get("/api/trip-item-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tripItemCategory.getId().intValue())));
    }

    @Test
    @Transactional
    public void getTripItemCategory() throws Exception {
        // Initialize the database
        tripItemCategoryRepository.saveAndFlush(tripItemCategory);

        // Get the tripItemCategory
        restTripItemCategoryMockMvc.perform(get("/api/trip-item-categories/{id}", tripItemCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tripItemCategory.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTripItemCategory() throws Exception {
        // Get the tripItemCategory
        restTripItemCategoryMockMvc.perform(get("/api/trip-item-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTripItemCategory() throws Exception {
        // Initialize the database
        tripItemCategoryRepository.saveAndFlush(tripItemCategory);
        int databaseSizeBeforeUpdate = tripItemCategoryRepository.findAll().size();

        // Update the tripItemCategory
        TripItemCategory updatedTripItemCategory = tripItemCategoryRepository.findOne(tripItemCategory.getId());
        // Disconnect from session so that the updates on updatedTripItemCategory are not directly saved in db
        em.detach(updatedTripItemCategory);

        restTripItemCategoryMockMvc.perform(put("/api/trip-item-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTripItemCategory)))
            .andExpect(status().isOk());

        // Validate the TripItemCategory in the database
        List<TripItemCategory> tripItemCategoryList = tripItemCategoryRepository.findAll();
        assertThat(tripItemCategoryList).hasSize(databaseSizeBeforeUpdate);
        TripItemCategory testTripItemCategory = tripItemCategoryList.get(tripItemCategoryList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTripItemCategory() throws Exception {
        int databaseSizeBeforeUpdate = tripItemCategoryRepository.findAll().size();

        // Create the TripItemCategory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTripItemCategoryMockMvc.perform(put("/api/trip-item-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tripItemCategory)))
            .andExpect(status().isCreated());

        // Validate the TripItemCategory in the database
        List<TripItemCategory> tripItemCategoryList = tripItemCategoryRepository.findAll();
        assertThat(tripItemCategoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTripItemCategory() throws Exception {
        // Initialize the database
        tripItemCategoryRepository.saveAndFlush(tripItemCategory);
        int databaseSizeBeforeDelete = tripItemCategoryRepository.findAll().size();

        // Get the tripItemCategory
        restTripItemCategoryMockMvc.perform(delete("/api/trip-item-categories/{id}", tripItemCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TripItemCategory> tripItemCategoryList = tripItemCategoryRepository.findAll();
        assertThat(tripItemCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TripItemCategory.class);
        TripItemCategory tripItemCategory1 = new TripItemCategory();
        tripItemCategory1.setId(1L);
        TripItemCategory tripItemCategory2 = new TripItemCategory();
        tripItemCategory2.setId(tripItemCategory1.getId());
        assertThat(tripItemCategory1).isEqualTo(tripItemCategory2);
        tripItemCategory2.setId(2L);
        assertThat(tripItemCategory1).isNotEqualTo(tripItemCategory2);
        tripItemCategory1.setId(null);
        assertThat(tripItemCategory1).isNotEqualTo(tripItemCategory2);
    }
}
