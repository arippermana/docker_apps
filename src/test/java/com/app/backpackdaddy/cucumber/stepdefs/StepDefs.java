package com.app.backpackdaddy.cucumber.stepdefs;

import com.app.backpackdaddy.BackpackdaddyApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = BackpackdaddyApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
