'use strict';

describe('Controller Tests', function() {

    describe('Trip Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockTrip, MockUser, MockCity, MockTripBaggage, MockTripPaymentMethod, MockUserBank, MockTripServiceFee, MockTripServiceFeeUnit, MockTripStatus;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockTrip = jasmine.createSpy('MockTrip');
            MockUser = jasmine.createSpy('MockUser');
            MockCity = jasmine.createSpy('MockCity');
            MockTripBaggage = jasmine.createSpy('MockTripBaggage');
            MockTripPaymentMethod = jasmine.createSpy('MockTripPaymentMethod');
            MockUserBank = jasmine.createSpy('MockUserBank');
            MockTripServiceFee = jasmine.createSpy('MockTripServiceFee');
            MockTripServiceFeeUnit = jasmine.createSpy('MockTripServiceFeeUnit');
            MockTripStatus = jasmine.createSpy('MockTripStatus');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Trip': MockTrip,
                'User': MockUser,
                'City': MockCity,
                'TripBaggage': MockTripBaggage,
                'TripPaymentMethod': MockTripPaymentMethod,
                'UserBank': MockUserBank,
                'TripServiceFee': MockTripServiceFee,
                'TripServiceFeeUnit': MockTripServiceFeeUnit,
                'TripStatus': MockTripStatus
            };
            createController = function() {
                $injector.get('$controller')("TripDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'backpackdaddyApp:tripUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
