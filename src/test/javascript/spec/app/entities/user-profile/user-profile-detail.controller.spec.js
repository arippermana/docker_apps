'use strict';

describe('Controller Tests', function() {

    describe('UserProfile Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockUserProfile, MockUser, MockGender, MockCountry, MockOccupation, MockCity, MockLanguage, MockCurrency;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockUserProfile = jasmine.createSpy('MockUserProfile');
            MockUser = jasmine.createSpy('MockUser');
            MockGender = jasmine.createSpy('MockGender');
            MockCountry = jasmine.createSpy('MockCountry');
            MockOccupation = jasmine.createSpy('MockOccupation');
            MockCity = jasmine.createSpy('MockCity');
            MockLanguage = jasmine.createSpy('MockLanguage');
            MockCurrency = jasmine.createSpy('MockCurrency');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'UserProfile': MockUserProfile,
                'User': MockUser,
                'Gender': MockGender,
                'Country': MockCountry,
                'Occupation': MockOccupation,
                'City': MockCity,
                'Language': MockLanguage,
                'Currency': MockCurrency
            };
            createController = function() {
                $injector.get('$controller')("UserProfileDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'backpackdaddyApp:userProfileUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
