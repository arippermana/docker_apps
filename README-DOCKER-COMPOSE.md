# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured docker services

### Applications and dependencies:
- backpackdaddy (monolith application)
- backpackdaddy's mysql database

### Additional Services:

